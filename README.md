## PowerBI ag-Grid Wrapper
'The Best Javascript Grid In the World'

https://www.ag-grid.com

A simple demo of the basic grid (ie. without row grouping or pivot) to test out PowerBI custom visualisations. 

Note: this wrapper exposes the Enterprise features of the grid as a trial, with the ability to add your own license key via PowerBI editor field settings.
To use the open source grid only, fork the repo & take out the enterprise library.

ToDo: 
* Matrix Data Mapping, to leverage the ag-grid's powerful row grouping & pivoting features.
* Field type detection for column formatting & grid filter. 
* Conditional formatting


