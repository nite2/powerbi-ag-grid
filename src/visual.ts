/*
*  Power BI Visual CLI
*
*  Copyright (c) Microsoft Corporation
*  All rights reserved.
*  MIT License
*
*  Permission is hereby granted, free of charge, to any person obtaining a copy
*  of this software and associated documentation files (the ''Software''), to deal
*  in the Software without restriction, including without limitation the rights
*  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
*  copies of the Software, and to permit persons to whom the Software is
*  furnished to do so, subject to the following conditions:
*
*  The above copyright notice and this permission notice shall be included in
*  all copies or substantial portions of the Software.
*
*  THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
*  THE SOFTWARE.
*/
'use strict';
import '@babel/polyfill';
import './../style/visual.less';
import powerbi from 'powerbi-visuals-api';
import VisualConstructorOptions = powerbi.extensibility.visual.VisualConstructorOptions;
import VisualUpdateOptions = powerbi.extensibility.visual.VisualUpdateOptions;
import IVisual = powerbi.extensibility.visual.IVisual;
import IVisualHost = powerbi.extensibility.visual.IVisualHost;
import EnumerateVisualObjectInstancesOptions = powerbi.EnumerateVisualObjectInstancesOptions;
import VisualObjectInstanceEnumeration = powerbi.EnumerateVisualObjectInstancesOptions;
import VisualObjectInstance = powerbi.VisualObjectInstance;


import {Grid, ColDef, GridOptions} from 'ag-grid-community'
import 'ag-grid-enterprise';
import {VisualSettings} from './settings';
import {LicenseManager} from 'ag-grid-enterprise/main';

'use strict';

const DEFAULT_DEBOUNCE_MS = 500;

const sideBar = {
    toolPanels: [
        {
            id: 'columns',
            labelDefault: 'Columns',
            labelKey: 'columns',
            iconKey: 'columns',
            toolPanel: 'agColumnsToolPanel',
        },
    ],
};

const defaultGridConfig = {
    sideBar,
    autoSizePadding: 0,
    cacheOverflowSize: 2,
    enableRangeSelection: true,
    enableRowGroup: true,
    enablePivot: true,
    enableValue: true,
    floatingFilter: true,
    maxBlocksInCache: 2,
    pagination: false,
    rowBuffer: 0,
    rowDeselection: true,
    rowSelection: 'multiple',
    suppressCellSelection: false,
    suppressHorizontalScroll: false,
    suppressRowClickSelection: true,
    defaultColDef: {
        enableRowGroup: true,
        enablePivot: true,
        enableValue: true,
        resizable: true,
        sortable: true,
        minWidth: 80,
        filter: 'agTextColumnFilter',
        suppressFilter: false,
        debounceMs: DEFAULT_DEBOUNCE_MS,
        filterParams: {
            suppressAndOrCondition: true,
        },
    } as ColDef,
    processCellForClipboard: value => {
        // Formatting datagrid copy cell using valueformatter for column
        // Missing feature of ag-grid - should do this by default.
        const valueFormatter = value.column['colDef'].valueFormatter;
        return valueFormatter ? valueFormatter(value) : value.value;
    },
    onFirstDataRendered: ({api}) => api.expandAll(),
    onGridReady: ({api}) => api.sizeColumnsToFit(),
} as GridOptions;

export class Visual implements IVisual {

    private visualSettings: VisualSettings;
    private readonly element;
    private gridOptions: GridOptions;

    constructor(options: VisualConstructorOptions) {
        this.element = options.element;
        this.element.classList.add('ag-theme-balham');
    }

    /**
     * Enumerates through the objects defined in the capabilities and adds the properties to the format pane
     *
     * @function
     * @param {EnumerateVisualObjectInstancesOptions} options - Map of defined objects
     */
    public enumerateObjectInstances(options) {
        const settings = this.visualSettings;
        // debugger;

        return VisualSettings.enumerateObjectInstances(settings, options);
    }

    public update(options: VisualUpdateOptions) {
        let dataView = options.dataViews['0'];
        const settings = this.visualSettings = VisualSettings.parse<VisualSettings>(dataView);
        if (settings.grid.gridKey) {
            LicenseManager.setLicenseKey(settings.grid.gridKey);
        }
        debugger;
        const columnDefs = dataView
            .table.columns
            .map(c => ({
                headerName: c.displayName,
                field: c.displayName.replace(/\s/g, '').toLowerCase(),
            } as ColDef));

        const rowData = dataView.table.rows.map(row =>
            row.map((item, i) => ({
                [columnDefs[i].field]: item
            })).reduce((a, c) => ({...a, ...c}), {})
        );

        if (!this.gridOptions) {
            this.gridOptions = {
                ...defaultGridConfig,
                floatingFilter: settings.grid.gridFilter,
                columnDefs: columnDefs,
                rowData: rowData
            } as GridOptions;

            new Grid(this.element, this.gridOptions);
        } else {
            let api = this.gridOptions.api;
            api.setColumnDefs(columnDefs);
            api.setRowData(rowData);
            api.sizeColumnsToFit();
        }

    }
}

